﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeoutHolder : MonoBehaviour {

	public float Timeout;

	public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
	{
		if (PhotonNetwork.isMasterClient)
		{
			//This is OUR player. We send our current position
			stream.SendNext(Timeout);
		}
		else
		{
			//This is someone else. We receive their position.
			Timeout = (float) stream.ReceiveNext();
		}
	}




}
