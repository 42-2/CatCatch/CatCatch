﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class settingMenu : MonoBehaviour {

	public Dropdown resolutionDropdown;
	Resolution[] resolutions;

	void Start(){
		resolutions = Screen.resolutions;
		resolutionDropdown.ClearOptions ();
		List<string> options = new List<string> ();
		for (int i = 0; i < resolutions.Length; i++) {
			options.Add (resolutions[i].width + " x " + resolutions[i].height);
		}
		resolutionDropdown.AddOptions (options);
		resolutionDropdown.RefreshShownValue();
	}

	public void SetResolution(int resolutionIndex){
		Resolution resolution = resolutions [resolutionIndex];
		Screen.SetResolution (resolution.width,resolution.height,Screen.fullScreen);
	}
	
	public void SetFullScreen(bool isFullScreen){
		Screen.fullScreen = isFullScreen;
	}
}
