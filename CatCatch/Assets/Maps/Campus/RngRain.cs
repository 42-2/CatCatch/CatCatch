﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Globalization;

public class RngRain : MonoBehaviour
{

	public GameObject Rain;
	public GameObject Cloud;
	private bool rainFallen=false;

	// Use this for initialization

	private void Enable()
	{
		Rain.SetActive(true);
		Cloud.SetActive(true);
	}

	private void Disable()
	{
		var rain = Rain.GetComponent<ParticleSystem>().emission;
		rain.enabled = false;
		var cloud=Cloud.GetComponent<ParticleSystem>().emission;
		cloud.enabled = false;
	}

	// Update is called once per frame
	void Update () {
		if (!rainFallen)
		{
			System.Random rnd = new System.Random();
			if (rnd.Next(10000) > 9990)
			{
				rainFallen = true;
				Enable();
				Invoke("Disable",60);
			}
		}
	}
}
