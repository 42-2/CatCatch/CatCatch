﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Globalization;

public class Trajectorys : MonoBehaviour
{
	public float speed;
	private int direction = 1;

	private void Start()
	{
		Invoke("Disable",120);
	}

	void Disable()
	{
		gameObject.SetActive(false);
	}

	void Update() {
		transform.Translate(Vector3.right * Time.deltaTime * speed * direction);
	}
}

