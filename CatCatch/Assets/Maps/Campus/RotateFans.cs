﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Globalization;

public class RotateFans : MonoBehaviour {

	public float rotation = 1f;
     
    void Update () {
    transform.Rotate (0,0,rotation * Time.deltaTime * 2f);
    }
}
