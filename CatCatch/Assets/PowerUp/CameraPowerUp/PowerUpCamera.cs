﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpCamera : MonoBehaviour {

    public GameObject Canvas;
    public GameObject pickupEffect;
    public float duration = 5f;

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Runner") ||other.CompareTag("Catcher"))
        {
            StartCoroutine(Pickup(other));
        }
    }

    IEnumerator Pickup(Collider player)
    {
        Instantiate(pickupEffect, transform.position, transform.rotation);
        var InstanceCanvas = Instantiate(Canvas, new Vector3(473, 303, -0), Quaternion.identity);

        GetComponent<MeshRenderer>().enabled = false;
        GetComponent<Collider>().enabled = false;


        yield return new WaitForSeconds(duration);



        Destroy(InstanceCanvas);
        //Debug.Log("DEAC");
        Destroy(gameObject);
    }
}
