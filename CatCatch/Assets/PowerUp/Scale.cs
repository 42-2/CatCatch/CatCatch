﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scale : MonoBehaviour
{
    public GameObject pickupEffectScale;
    public float durationofscale = 4f;
    public float multiplierscale = 4f;
    private Transform playerTransform;

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Runner") || other.CompareTag("Catcher")) 
        {
            StartCoroutine(PickupScale(other));
        }
    }

    IEnumerator PickupScale(Collider player)
    {


        Instantiate(pickupEffectScale, transform.position, transform.rotation);

        player.transform.localScale /= multiplierscale;
//        player.transform.localScale /= multiplierscale;

        gameObject.GetComponent<MeshRenderer>().enabled = false;
        gameObject.GetComponent<Collider>().enabled = false;


        yield return new WaitForSeconds(durationofscale);

        //player.transform.localScale *= multiplierscale;
        player.transform.localScale *= multiplierscale;
        Destroy(gameObject);
    }
}
