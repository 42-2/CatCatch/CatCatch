﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomCatScript : MonoBehaviour {

    public GameObject CustomCatIcon;
    public GameObject CustomCatButton;
    private int NumberGameMode;


    public GameObject player;
    public Renderer Texture;
    public GameObject Lunette;
    public GameObject CigaretteElec;
    public GameObject Noeud;
    public GameObject Casquette;
    public GameObject Aile;
    public GameObject Bière;
    public GameObject Pc;
    public GameObject Youtube;

    public Material Mat1;
    public Material Mat2;
    public Material Mat3;
    public Material Mat4;

    public Quaternion rotatePlus;
    public Quaternion rotateMoins;
    private int itemHead;
    private int itemBody;
    private int itemTexture;


    public void Start()
    {
        itemHead = 0;
        itemBody = 0;
        itemTexture = 0;
    }

    public void NextItemHead()
    {
        itemHead += 1;
    }

    public void PreviousItemHead()
    {
        itemHead -= 1;
    }

    public void NextItemBody()
    {
        itemBody += 1;
    }

    public void PreviousItemBody()
    {
        itemBody -= 1;
    }

    public void NextItemTexture()
    {
        itemTexture += 1;
    }

    public void PreviousItemTexture()
    {
        itemTexture -= 1;
    }

    public void RotationPlus()
    {
         player.transform.rotation *= rotatePlus;
    }

    public void RotationMoins()
    {
        player.transform.rotation *= rotateMoins; 
    }

    

    public void Update()
    {
        //ITEMHEAD
        if (itemHead == 0)
        {
            Lunette.SetActive(false);
            CigaretteElec.SetActive(false);
            Casquette.SetActive(false);
            Noeud.SetActive(false);
        }
        if (itemHead == 1)
        {
            Lunette.SetActive(true);
            CigaretteElec.SetActive(false);
            Casquette.SetActive(false);
            Noeud.SetActive(false);
        }
        if (itemHead == 2)
        {
            Lunette.SetActive(false);
            CigaretteElec.SetActive(true);
            Casquette.SetActive(false);
            Noeud.SetActive(false);
        }
        if (itemHead == 3)
        {
            Lunette.SetActive(false);
            CigaretteElec.SetActive(false);
            Casquette.SetActive(true);
            Noeud.SetActive(false);
        }
        if (itemHead == 4)
        {
            Lunette.SetActive(false);
            CigaretteElec.SetActive(false);
            Casquette.SetActive(false);
            Noeud.SetActive(true);
        }

        if (itemHead > 4)
        {
            itemHead = 0;
        }
        if (itemHead < 0)
        {
            itemHead = 4;
        }

        //ITEM BODY 
        if (itemBody == 0)
        {
            Aile.SetActive(false);
            Bière.SetActive(false);
            Pc.SetActive(false);
            Youtube.SetActive(false);
        }
        if (itemBody == 1)
        {
            Aile.SetActive(true);
            Bière.SetActive(false);
            Pc.SetActive(false);
            Youtube.SetActive(false);
        }
        if (itemBody == 2)
        {
            Aile.SetActive(false);
            Bière.SetActive(true);
            Pc.SetActive(false);
            Youtube.SetActive(false);
        }
        if (itemBody == 3)
        {
            Aile.SetActive(false);
            Bière.SetActive(false);
            Pc.SetActive(true);
            Youtube.SetActive(false);
        }
        if (itemBody == 4)
        {
            Aile.SetActive(false);
            Bière.SetActive(false);
            Pc.SetActive(false);
            Youtube.SetActive(true);
        }

        if (itemBody > 4)
        {
            itemBody = 0;
        }
        if (itemBody < 0)
        {
            itemBody = 4;
        }

        //ITEMTEXTURE
        if (itemTexture == 0)
        {
            Texture.material = Mat1;
        }

        if (itemTexture == 1)
        {
            Texture.material = Mat2;
        }

        if (itemTexture == 2)
        {
            Texture.material = Mat3;
        }

        if (itemTexture == 3)
        {
            Texture.material = Mat4;
        }

        if (itemTexture > 3)
        {
            itemTexture = 0;
        }

        if (itemTexture < 0)
        {
            itemTexture = 3;
        }

        PlayerPrefs.SetInt("itemTexture", itemTexture);
        PlayerPrefs.SetInt("itemHead", itemHead);
        PlayerPrefs.SetInt("itemBody", itemBody);


        NumberGameMode = PlayerPrefs.GetInt("GameMode");
        if(NumberGameMode == 1)
        {
            CustomCatIcon.gameObject.SetActive(true);
            CustomCatButton.gameObject.SetActive(true);
        }
        if(NumberGameMode == 2 || NumberGameMode == 3)
        {
            CustomCatIcon.gameObject.SetActive(false);
            CustomCatButton.gameObject.SetActive(false);
        }
    }


}
