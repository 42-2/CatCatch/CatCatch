﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    private const string SiteUrl = "http://www.catcatch.ovh/api/";

    public TMP_InputField Username;
    public TMP_InputField Password;
    public TMP_Text Error;
    private string _connected = "";
    private string _username;
    private string _password;


    public void SetGameMode(int mode)
    {
        PlayerPrefs.SetInt("GameMode", mode);
    }

    public void SetNumberOfPlayers(int number)
    {
        PlayerPrefs.SetInt("NumberOfPlayers", number);
    }

    public void Play(string catChosen)
    {
        PlayerPrefs.SetString("PlayerName", catChosen);
        switch (PlayerPrefs.GetInt("GameMode"))
        {
            case 1:
                SceneManager.LoadScene("Solo");
                break;
            case 2:
                SceneManager.LoadScene("Multiplayer");
                break;
            case 3:
                PlayerPrefs.SetInt("NumberOfplayers", 10);
                SceneManager.LoadScene("BattleRoyale");
                break;
        }
    }

    public void Login()
    {
        //Debug.Log("loggin ing");
        _username = Username.text;
        _password = Password.text;
        StartCoroutine(RequestLogin());
    }


    public void Register()
    {
        //Debug.Log("registerimg");
        _username = Username.text;
        _password = Password.text;
        StartCoroutine(RequestRegister());
    }

    IEnumerator RequestLogin()
    {
        var form = new WWWForm();
        form.AddField("username", _username);
        form.AddField("password", _password);

        using (var www = UnityWebRequest.Post(SiteUrl + "login.php", form))
        {
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                //Debug.Log(www.error);
                transform.parent.Find("PlayerSelectionMenu").gameObject.SetActive(false);
                transform.parent.Find("LoginMenu").gameObject.SetActive(true);
                Username.text = "";
                Password.text = "";
                Error.gameObject.transform.parent.gameObject.SetActive(true);
                Error.text = www.error;
            }
            else
            {
                var response = JsonUtility.FromJson<JsonConnect>(www.downloadHandler.text);
                if (response.status_message == "Credentials correct")
                {
                    PlayerPrefs.SetString("Username", _username);
                    PlayerPrefs.SetString("Password", _password);
                }
                else
                {
                    //Debug.Log(response.status_message);
                    transform.parent.Find("PlayerSelectionMenu").gameObject.SetActive(false);
                    transform.parent.Find("LoginMenu").gameObject.SetActive(true);
                    Username.text = "";
                    Password.text = "";
                    Error.gameObject.transform.parent.gameObject.SetActive(true);
                    Error.text = response.status_message;

                }
            }
        }
    }

    IEnumerator RequestRegister()
    {
        var form = new WWWForm();
        form.AddField("username", _username);
        form.AddField("password", _password);

        using (var www = UnityWebRequest.Post(SiteUrl + "register.php", form))
        {
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                //Debug.Log(www.error);
                transform.parent.Find("PlayerSelectionMenu").gameObject.SetActive(false);
                transform.parent.Find("LoginMenu").gameObject.SetActive(true);
                Username.text = "";
                Password.text = "";
                Error.gameObject.transform.parent.gameObject.SetActive(true);
                Error.text = www.error;
            }
            else
            {
                var response = JsonUtility.FromJson<JsonRegister>(www.downloadHandler.text);
                if (response.status_message == "You registered successfully, you can now log yourself in")
                {
                    //Debug.Log(response.status_message);
                    transform.parent.Find("PlayerSelectionMenu").gameObject.SetActive(false);
                    transform.parent.Find("LoginMenu").gameObject.SetActive(true);
                    Error.gameObject.transform.parent.gameObject.SetActive(true);
                    Error.text = response.status_message;
                    //Username.text = response.data;
                    Password.text = "";
                }
                else
                {
                    //Debug.Log(response.status_message);
                    transform.parent.Find("PlayerSelectionMenu").gameObject.SetActive(false);
                    transform.parent.Find("LoginMenu").gameObject.SetActive(true);
                    Username.text = "";
                    Password.text = "";
                    Error.gameObject.transform.parent.gameObject.SetActive(true);
                    Error.text = response.status_message;
                }
            }
        }
    }

    public void QuitGame()
    {
        //Debug.Log("QUIT");
        Application.Quit();
    }
}

public class JsonConnect
{
    public string status;
    public string status_message;
    public string data;
}

public class JsonRegister
{
    public string status;
    public string status_message;
    public string data;
}