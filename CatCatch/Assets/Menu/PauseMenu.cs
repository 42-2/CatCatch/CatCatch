﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Audio;


public class PauseMenu : MonoBehaviour {

    public bool GameIsPaused = false;
    public AudioMixer audioMixer;
    public GameObject pauseMenuUI;
    private Animator _anim;
    private float _oldSpeed, _oldJumpSpeed;
    public GameObject scoreBoardUI;
    private NetworkManager _networkManager;



    private void Start()
    {
        _anim = GetComponent<Animator>();
        _oldSpeed = GetComponent<PlayerMovement>().Speed;
        _oldJumpSpeed = GetComponent<PlayerMovement>().JumpSpeed;
        _networkManager = FindObjectOfType<NetworkManager>();
    }

    void Update () {

        if(Input.GetKeyDown(KeyCode.Escape))
        {
            if (GameIsPaused)
            {
                Resume();
            }
            else
            {
                Pause();
            }
        }



    }

    public void Resume()
    {
        pauseMenuUI.SetActive(false);
        Cursor.visible = false;
        GetComponent<PlayerCatching>().enabled = true;
        GetComponent<MouseLook>().enabled = true;
        GetComponent<PlayerMovement>().JumpSpeed = _oldJumpSpeed;
        GetComponent<PlayerMovement>().Speed = _oldSpeed;
        transform.Find("Main Camera").gameObject.GetComponent<MouseLook>().enabled = true;
        GameIsPaused = false;
        scoreBoardUI.gameObject.SetActive(true);
        var runners = GameObject.FindGameObjectsWithTag("Runner");
        var catchers = GameObject.FindGameObjectsWithTag("Catcher");
        var customs = GameObject.FindGameObjectsWithTag("Custom_Runner");
        foreach (var r in runners)
        {
            if(r.GetComponent<ScoreActivater>() != null)
                r.GetComponent<ScoreActivater>().scoreboard.SetActive(true);
        }
        foreach (var r in catchers)
        {
            if(r.GetComponent<ScoreActivater>() != null)
                r.GetComponent<ScoreActivater>().scoreboard.SetActive(true);
        }
        foreach (var r in customs)
        {
            if(r.GetComponent<ScoreActivater>() != null)
                r.GetComponent<ScoreActivater>().scoreboard.SetActive(true);
        }

    }

    void Pause()
    {
        if (!_networkManager.GameIsOver)
        {
            pauseMenuUI.SetActive(true);
            Cursor.visible = true;

       
            _anim.SetFloat("Speed", 0f);
        
            GetComponent<PlayerCatching>().enabled = false;
       
            GetComponent<PlayerMovement>().JumpSpeed = 0;
            GetComponent<PlayerMovement>().Speed = 0;
            GetComponent<MouseLook>().enabled = false;
            transform.Find("Main Camera").gameObject.GetComponent<MouseLook>().enabled = false;
            GameIsPaused = true;
            scoreBoardUI.gameObject.SetActive(false);
            var runners = GameObject.FindGameObjectsWithTag("Runner");
            var catchers = GameObject.FindGameObjectsWithTag("Catcher");
            var customs = GameObject.FindGameObjectsWithTag("Custom_Runner");
            foreach (var r in runners)
            {
                if(r.GetComponent<ScoreActivater>() != null)
                    r.GetComponent<ScoreActivater>().scoreboard.SetActive(false);
            }
            foreach (var r in catchers)
            {
                if(r.GetComponent<ScoreActivater>() != null)
                    r.GetComponent<ScoreActivater>().scoreboard.SetActive(false);
            }
            foreach (var r in customs)
            {
                if(r.GetComponent<ScoreActivater>() != null)
                    r.GetComponent<ScoreActivater>().scoreboard.SetActive(false);
            }
        }
    }

    public void Quit()
    {
        Cursor.visible = true ;
        GetComponent<PlayerCatching>().enabled = true;
        GetComponent<MouseLook>().enabled = true;
        GetComponent<PlayerMovement>().JumpSpeed = _oldJumpSpeed;
        GetComponent<PlayerMovement>().Speed = _oldSpeed;
        transform.Find("Main Camera").gameObject.GetComponent<MouseLook>().enabled = true;
        PhotonNetwork.LeaveRoom();
        PhotonNetwork.Disconnect();
        SceneManager.LoadScene("Menu");
    }

    public void SetVolume(float volume)
    {
        audioMixer.SetFloat("Volume", volume);
    }
}
