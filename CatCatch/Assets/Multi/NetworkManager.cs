﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;
using System;
using System.Linq.Expressions;
using ProBuilder2.Common;

public class NetworkManager : MonoBehaviour
{
	public bool GameIsOver = false;
    private const string GameVersion = "v2.0.1";
	private GameObject[] _lobbyWalls;
	private SpawnPoint[] _spawnPoints;
	public string winners = "";
	public string bestplayer;
	private SiteLink _siteLink;
	private bool dataSent = false;
	public GameObject timerDisplay;

	private readonly string[] _cats = { "Cedric_Cat", "Chelsea_Cat", "Edgar_Cat", "HamdiJmal_Cat", "Hervot_Cat",
		"Jakemain_Cat", "Jean_Cat", "Julien_Cat", "Marc_Cat", "Maxime_Cat", "Merle_Cat", "Paul_Cat", "PingPong_Cat",
		"Sticker_Cat", "Sylvain_Cat", "Tinder_Cat" };

	private readonly string[] _catchers = { "HamdiJmal_Cat", "Hervot_Cat", "Jakemain_Cat", "Sticker_Cat" };

	private readonly string[] _runners = { "Cedric_Cat", "Chelsea_Cat", "Edgar_Cat", "Jean_Cat", "Julien_Cat",
		"Marc_Cat", "Maxime_Cat", "Merle_Cat", "Paul_Cat", "PingPong_Cat", "Sylvain_Cat", "Tinder_Cat"};
	private int _numberOfPlayers;
	public bool GameStarted = false;

	public GameObject HoldingCamera;
	public GameObject CrossHair;
	public bool Offline;
	public bool BattleRoyale;
	public GameObject LobbySpawn;
	public GameObject PowerUps;
	public List<GameObject> SpawnPointsRunners;
	public List<GameObject> SpawnPointsCatchers;
	public float Timeout;
	public float GameDuration = 360;
	private int waiter = 0;
	public TimeoutHolder timeoutHolder;
    public float TimeLeft;
	private bool customCatsActivated = false;

	// Use this for initialization
	private void Start ()
	{
		_siteLink = FindObjectOfType<SiteLink>();
		_spawnPoints = FindObjectsOfType<SpawnPoint>();
		_lobbyWalls = GameObject.FindGameObjectsWithTag("InvisibleWall");
		PhotonNetwork.playerName = PlayerPrefs.GetString("Username");
		PhotonNetwork.player.SetScore(0);
		_numberOfPlayers = PlayerPrefs.GetInt("NumberOfPlayers");
		Connect();
	}

	

	private void Connect()
	{
		if (Offline)
		{
			PhotonNetwork.offlineMode = true;
			OnJoinedLobby();
		}
		else
		{
			PhotonNetwork.ConnectUsingSettings(GameVersion);
		}
	}

	private void OnJoinedLobby()
	{
		var expectedCustomRoomProperties = BattleRoyale ? new ExitGames.Client.Photon.Hashtable() {{"mod", 1}} : new ExitGames.Client.Photon.Hashtable() {{"mod", 2}};
		PhotonNetwork.JoinRandomRoom(expectedCustomRoomProperties, (byte) _numberOfPlayers);
	}

	private static string RandomString(int length)
	{
		var random = new System.Random();
		const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		return new string(Enumerable.Repeat(chars, length)
			.Select(s => s[random.Next(s.Length)]).ToArray());
	}

	private void OnPhotonRandomJoinFailed()
	{
		var room = new RoomOptions {MaxPlayers = (byte) _numberOfPlayers};
		room.CustomRoomPropertiesForLobby = new []{ "mod" };
		room.CustomRoomProperties = BattleRoyale ? new ExitGames.Client.Photon.Hashtable() {{"mod", 1}} : new ExitGames.Client.Photon.Hashtable() {{"mod", 2}};
		var lobby = new TypedLobby();
		PhotonNetwork.JoinOrCreateRoom(RandomString(8), room, lobby);
	}

	private void OnPhotonCreateRoomFailed()
	{
		OnPhotonRandomJoinFailed();
	}

	private void OnPhotonJoinRoomFailed()
	{
		OnPhotonRandomJoinFailed();
	}

	private void OnJoinedRoom()
	{
		if (Offline) SpawnSolo();
		else SpawnInLobby();
	}

	private void SpawnInLobby()
	{
		var playerGO = SpawnPlayer(mySpawnPoint: LobbySpawn);
		playerGO.transform.Find("SpellUI").gameObject.SetActive(false);
		playerGO.GetComponent<Spell>().enabled = false;
		playerGO.GetComponent<PlayerCatching>().enabled = false;
		foreach (var wall in _lobbyWalls) wall.SetActive(true);
        PowerUps.gameObject.SetActive(false);
	}

	private void SpawnSolo()
	{
		var pos = Array.IndexOf(_catchers, PlayerPrefs.GetString("PlayerName")); // if player is a catcher, pos will be greater than -1

		if (pos > -1)
		{
			for (var i = 0; i < 3 * _numberOfPlayers / 4; i++) SpawnPlayer(true, _runners, SpawnPointsRunners[UnityEngine.Random.Range(0, SpawnPointsRunners.Count)]);
			SpawnPlayer(mySpawnPoint: SpawnPointsCatchers[UnityEngine.Random.Range(0, SpawnPointsCatchers.Count)]);
		}
		else
		{
			for (var i = 0; i < 3 * _numberOfPlayers / 4 - 1; i++) SpawnPlayer(true, _runners, SpawnPointsRunners[UnityEngine.Random.Range(0, SpawnPointsRunners.Count)]);
			for (var i = 0; i < _numberOfPlayers / 4; i++) SpawnPlayer(true, _catchers, SpawnPointsCatchers[UnityEngine.Random.Range(0, SpawnPointsCatchers.Count)]);
			SpawnPlayer(mySpawnPoint: SpawnPointsRunners[UnityEngine.Random.Range(0, SpawnPointsRunners.Count)]);
		}
	}

	private GameObject SpawnPlayer(bool ai = false, string[] cats = null, GameObject mySpawnPoint = null)
	{
		if (mySpawnPoint == null) mySpawnPoint = _spawnPoints[UnityEngine.Random.Range(0, _spawnPoints.Length)].gameObject;
		GameObject playerGO;

		if (!ai && PlayerPrefs.HasKey("PlayerName"))
		{
           
                playerGO = PhotonNetwork.Instantiate(PlayerPrefs.GetString("PlayerName"), mySpawnPoint.transform.position,
                  mySpawnPoint.transform.rotation, 0);
            if (playerGO == GameObject.FindGameObjectWithTag("Custom_Runner"))
            {
                var itemTexture = PlayerPrefs.GetInt("itemTexture");
                var itemBody = PlayerPrefs.GetInt("itemBody");
                var itemHead = PlayerPrefs.GetInt("itemHead");
                switch (itemBody)
                {
	                case 1:
		                playerGO.transform.Find("Rig_Cat_Lite").gameObject.transform.Find("Master").gameObject.transform.Find("BackBone_03").gameObject.transform.Find("BackBone_02").gameObject.transform.Find("Aile").gameObject.SetActive(true);
		                break;
	                case 2:
		                playerGO.transform.Find("Rig_Cat_Lite").gameObject.transform.Find("Master").gameObject.transform.Find("BackBone_03").gameObject.transform.Find("BackBone_02").gameObject.transform.Find("bière").gameObject.SetActive(true);
		                break;
	                case 3:
		                playerGO.transform.Find("Rig_Cat_Lite").gameObject.transform.Find("Master").gameObject.transform.Find("BackBone_03").gameObject.transform.Find("BackBone_02").gameObject.transform.Find("Pc_Hervot").gameObject.SetActive(true);
		                break;
	                case 4:
		                playerGO.transform.Find("Rig_Cat_Lite").gameObject.transform.Find("Master").gameObject.transform.Find("BackBone_03").gameObject.transform.Find("BackBone_02").gameObject.transform.Find("youtube").gameObject.SetActive(true);
		                break;
                }

	            switch (itemHead)
	            {
		            case 1:
			            playerGO.transform.Find("Rig_Cat_Lite").gameObject.transform.Find("Master").gameObject.transform.Find("BackBone_03").gameObject.transform.Find("BackBone_02").gameObject.transform.Find("BackBone_01").gameObject.transform.Find("Neck").gameObject.transform.Find("Head").gameObject.transform.Find("lunette_edgar").gameObject.SetActive(true);
			            break;
		            case 2:
			            playerGO.transform.Find("Rig_Cat_Lite").gameObject.transform.Find("Master").gameObject.transform.Find("BackBone_03").gameObject.transform.Find("BackBone_02").gameObject.transform.Find("BackBone_01").gameObject.transform.Find("Neck").gameObject.transform.Find("Head").gameObject.transform.Find("cigarette_elec").gameObject.SetActive(true);
			            break;
		            case 3:
			            playerGO.transform.Find("Rig_Cat_Lite").gameObject.transform.Find("Master").gameObject.transform.Find("BackBone_03").gameObject.transform.Find("BackBone_02").gameObject.transform.Find("BackBone_01").gameObject.transform.Find("Neck").gameObject.transform.Find("Head").gameObject.transform.Find("casquette_maxime").gameObject.SetActive(true);
			            break;
		            case 4:
			            playerGO.transform.Find("Rig_Cat_Lite").gameObject.transform.Find("Master").gameObject.transform.Find("BackBone_03").gameObject.transform.Find("BackBone_02").gameObject.transform.Find("BackBone_01").gameObject.transform.Find("Neck").gameObject.transform.Find("Head").gameObject.transform.Find("noeud_papillon").gameObject.SetActive(true);
			            break;
	            }

	            switch (itemTexture)
	            {
		            case 0:
			            playerGO.transform.Find("Cat_Lite_White").gameObject.SetActive(true);
			            break;
		            case 1:
			            playerGO.transform.Find("Cat_Lite_Red").gameObject.SetActive(true);
			            break;
		            case 2:
			            playerGO.transform.Find("Cat_Lite_Blue").gameObject.SetActive(true);
			            break;
		            case 3:
			            playerGO.transform.Find("Cat_Lite_Green").gameObject.SetActive(true);
			            break;
	            }
            }
            
		}
		else
		{
			if (cats == null) cats = _cats;
			playerGO = PhotonNetwork.Instantiate(cats[UnityEngine.Random.Range(0, cats.Length)], mySpawnPoint.transform.position,
				mySpawnPoint.transform.rotation, 0);
		}

		//////Debug.Log(playerGO);

		playerGO.GetComponent<Spell>().enabled = Offline;

        if (ai)
		{
			playerGO.GetComponent<CharacterController>().enabled = false;
			playerGO.AddComponent<Rigidbody>();
			playerGO.GetComponent<Rigidbody>().isKinematic = true;
			playerGO.GetComponent<MeshCollider>().enabled = true;
			playerGO.GetComponent<NavMeshAgent>().enabled = true;
			playerGO.GetComponent<EnnemyController>().enabled = true;
			var runners = GameObject.FindGameObjectsWithTag("Runner");
			var catchers= GameObject.FindGameObjectsWithTag("Catcher");
            var custom = GameObject.FindGameObjectsWithTag("Custom_Runner");


            foreach (var potato in runners)
			{
				potato.transform.Find("CanvasHealthBar").gameObject.SetActive(false);
			}

			foreach (var apple in catchers)
			{
				apple.transform.Find("CanvasHealthBar").gameObject.SetActive(false);
			}
            foreach (var lol in custom)
            {
                lol.transform.Find("CanvasHealthBar").gameObject.SetActive(false);
            }
        }
       
        
		else
		{
            playerGO.GetComponent<PauseMenu>().enabled = true;
			playerGO.GetComponent<MouseLook>().enabled = true;
			playerGO.GetComponent<PlayerMovement>().enabled = true;
			HoldingCamera.SetActive(false);
			playerGO.transform.Find("Main Camera").gameObject.SetActive(true);
			playerGO.transform.Find("Main Camera").gameObject.GetComponent<MouseLook>().enabled = true;
            playerGO.transform.Find("Main Camera").gameObject.GetComponent<CameraCollider>().enabled = true;
            playerGO.transform.Find("Cam2").gameObject.GetComponent<CameraCollider>().enabled = true;
            playerGO.transform.Find("Cam2").gameObject.GetComponent<MouseLook>().enabled = true;
			CrossHair.SetActive(true);
			playerGO.transform.Find("SpellUI").gameObject.SetActive(true);
			playerGO.transform.Find("CanvasPauseMenu").gameObject.SetActive(true);
			playerGO.transform.Find("CanvasUserName").gameObject.SetActive(false);

			if (BattleRoyale)
			{
				playerGO.GetComponent<PlayerBattleRoyale>().enabled = true;
				playerGO.GetComponent<PlayerCatching>().enabled = false;
			}
			else
			{
				var runners = GameObject.FindGameObjectsWithTag("Runner");
				var catchers= GameObject.FindGameObjectsWithTag("Catcher");
                var custom = GameObject.FindGameObjectsWithTag("Custom_Runner");


                foreach (var potato in runners)
				{
					potato.transform.Find("CanvasHealthBar").gameObject.SetActive(false);
				}

				foreach (var apple in catchers)
				{
					apple.transform.Find("CanvasHealthBar").gameObject.SetActive(false);
				}
                foreach (var lol in custom)
                {
                    lol.transform.Find("CanvasHealthBar").gameObject.SetActive(false);
                }
            }
		}

		return playerGO;
	}

	private void StartGame()
	{
        PowerUps.gameObject.SetActive(true);
		//////Debug.Log("Start game");
		GameStarted = true;
		if(BattleRoyale)
			timerDisplay.SetActive(false);
		var currentCatchers = GameObject.FindGameObjectsWithTag("Catcher");
		var currentRunners = GameObject.FindGameObjectsWithTag("Runner");
		var players = currentCatchers.Concat(currentRunners);
		foreach (var player in players)
		{
			if (!player.GetComponent<PlayerMovement>().enabled) continue;
			player.transform.Find("SpellUI").gameObject.SetActive(true);
			player.GetComponent<Spell>().enabled = true;
			player.GetComponent<PlayerCatching>().enabled = true;
			player.transform.position = player.CompareTag("Catcher")
				? SpawnPointsCatchers[UnityEngine.Random.Range(0, SpawnPointsCatchers.Count)].transform.position
				: SpawnPointsRunners[UnityEngine.Random.Range(0, SpawnPointsRunners.Count)].transform.position;
			player.transform.rotation = player.CompareTag("Catcher")
				? SpawnPointsCatchers[UnityEngine.Random.Range(0, SpawnPointsCatchers.Count)].transform.rotation
				: SpawnPointsRunners[UnityEngine.Random.Range(0, SpawnPointsRunners.Count)].transform.rotation;
		}
		foreach (var wall in _lobbyWalls) wall.SetActive(false);
	} 

	private void OnEnable()
	{
		PhotonNetwork.OnEventCall += this.OnEvent;
	}

	private void OnDisable()
	{
		PhotonNetwork.OnEventCall -= this.OnEvent;
	}

	private void OnEvent(byte eventCode, object content, int senderid)
	{
		switch (eventCode)
		{
			case 1:
				StartGame();
				GameStarted = true;
				break;
		}
	}

    public void TimeSolo()
    {
        if (Offline)
        {
            waiter++;
            if (waiter >= 30)
            {
                var runners = GameObject.FindGameObjectsWithTag("Runner");
                var catchers = GameObject.FindGameObjectsWithTag("Catcher");
                var custom = GameObject.FindGameObjectsWithTag("Custom_Runner");

                foreach (var potato in runners)
                {
                    potato.transform.Find("CanvasHealthBar").gameObject.SetActive(false);
                    potato.GetComponent<Spell>().enabled = true;
                }

                foreach (var apple in catchers)
                {
                    apple.transform.Find("CanvasHealthBar").gameObject.SetActive(false);
                    apple.GetComponent<Spell>().enabled = true;
                }

                foreach (var lol in custom)
                {
                    lol.transform.Find("CanvasHealthBar").gameObject.SetActive(false);
                    lol.GetComponent<Spell>().enabled = true;
                }
                waiter = 0;
            }
            Timeout += Time.deltaTime;
            timeoutHolder.Timeout = Timeout;
            GameIsOver = IsGameFinished();
            return;
        }
    }



	// Update is called once per frame
	void Update ()
	{
		if (customCatsActivated)
		{
			var customCats = GameObject.FindGameObjectsWithTag("Custom_Runner");
			foreach (var cat in customCats)
			{
				var itemBody = cat.GetComponent<CustomCatVars>().itemBody;
				var itemHead = cat.GetComponent<CustomCatVars>().itemHead;
				var itemTexture = cat.GetComponent<CustomCatVars>().itemTexture;
				switch (itemBody)
        	        {
	    	            case 1:
			                cat.transform.Find("Rig_Cat_Lite").gameObject.transform.Find("Master").gameObject.transform.Find("BackBone_03").gameObject.transform.Find("BackBone_02").gameObject.transform.Find("Aile").gameObject.SetActive(true);
			                break;
	    	            case 2:
			                cat.transform.Find("Rig_Cat_Lite").gameObject.transform.Find("Master").gameObject.transform.Find("BackBone_03").gameObject.transform.Find("BackBone_02").gameObject.transform.Find("bière").gameObject.SetActive(true);
			                break;
	    	            case 3:
			                cat.transform.Find("Rig_Cat_Lite").gameObject.transform.Find("Master").gameObject.transform.Find("BackBone_03").gameObject.transform.Find("BackBone_02").gameObject.transform.Find("Pc_Hervot").gameObject.SetActive(true);
			                break;
	    	            case 4:
			                cat.transform.Find("Rig_Cat_Lite").gameObject.transform.Find("Master").gameObject.transform.Find("BackBone_03").gameObject.transform.Find("BackBone_02").gameObject.transform.Find("youtube").gameObject.SetActive(true);
			                break;
        	        }
        	
	    	        switch (itemHead)
	    	        {
			            case 1:
				            cat.transform.Find("Rig_Cat_Lite").gameObject.transform.Find("Master").gameObject.transform.Find("BackBone_03").gameObject.transform.Find("BackBone_02").gameObject.transform.Find("BackBone_01").gameObject.transform.Find("Neck").gameObject.transform.Find("Head").gameObject.transform.Find("lunette_edgar").gameObject.SetActive(true);
				            break;
			            case 2:
				            cat.transform.Find("Rig_Cat_Lite").gameObject.transform.Find("Master").gameObject.transform.Find("BackBone_03").gameObject.transform.Find("BackBone_02").gameObject.transform.Find("BackBone_01").gameObject.transform.Find("Neck").gameObject.transform.Find("Head").gameObject.transform.Find("cigarette_elec").gameObject.SetActive(true);
				            break;
			            case 3:
				            cat.transform.Find("Rig_Cat_Lite").gameObject.transform.Find("Master").gameObject.transform.Find("BackBone_03").gameObject.transform.Find("BackBone_02").gameObject.transform.Find("BackBone_01").gameObject.transform.Find("Neck").gameObject.transform.Find("Head").gameObject.transform.Find("casquette_maxime").gameObject.SetActive(true);
				            break;
			            case 4:
				            cat.transform.Find("Rig_Cat_Lite").gameObject.transform.Find("Master").gameObject.transform.Find("BackBone_03").gameObject.transform.Find("BackBone_02").gameObject.transform.Find("BackBone_01").gameObject.transform.Find("Neck").gameObject.transform.Find("Head").gameObject.transform.Find("noeud_papillon").gameObject.SetActive(true);
				            break;
	    	        }
        	
	    	        switch (itemTexture)
	    	        {
			            case 0:
				            cat.transform.Find("Cat_Lite_White").gameObject.SetActive(true);
				            break;
			            case 1:
				            cat.transform.Find("Cat_Lite_Red").gameObject.SetActive(true);
				            break;
			            case 2:
				            cat.transform.Find("Cat_Lite_Blue").gameObject.SetActive(true);
				            break;
			            case 3:
				            cat.transform.Find("Cat_Lite_Green").gameObject.SetActive(true);
				            break;
	    	        }
			}

			customCatsActivated = true;
		}
		
        if (PhotonNetwork.isMasterClient)
		{
            if (Timeout > -1 && !GameStarted)
            {
                Timeout -= Time.deltaTime;
            }
			if (!GameStarted && (Timeout < 0 || PhotonNetwork.playerList.Length == _numberOfPlayers))
			{
				GameStarted = true;
				PhotonNetwork.room.IsOpen = false;
				Timeout = 0;
				byte eventCode = 1;
				var content = new byte[] { };
				var parameters = new RaiseEventOptions {Receivers = ReceiverGroup.All};
				PhotonNetwork.RaiseEvent(eventCode, content, true, parameters);
			}

			if (GameStarted) Timeout += Time.deltaTime;
			
			timeoutHolder.Timeout = Timeout;
		}
		else
		{
			Timeout = timeoutHolder.Timeout;
		}

        TimeLeft = GameStarted ? GameDuration - Timeout : Timeout;

		GameIsOver = GameStarted && IsGameFinished();
		if (GameIsOver && !dataSent)
		{
			bestplayer = GetHighestScorePlayer();
			if (PhotonNetwork.isMasterClient)
			{
				var runners = GameObject.FindGameObjectsWithTag("Runner");
				var catchers = GameObject.FindGameObjectsWithTag("Catcher");
				var custom = GameObject.FindGameObjectsWithTag("Custom_Runner");
				foreach (var player in runners.Concat(catchers).Concat(custom))
				{
					_siteLink.SendScores(player.GetComponent<NetworkCharacter>().Username,
						Offline ? 1 : BattleRoyale ? 3 : 2, player.GetComponent<NetworkCharacter>().Score,
						player.GetComponent<WinOrLoose>().winner);
				}
				Timeout = GameDuration;
			}
			dataSent = true;
		}


		waiter++;
		if (!BattleRoyale && waiter >= 30)
		{
			var runners = GameObject.FindGameObjectsWithTag("Runner");
			var catchers= GameObject.FindGameObjectsWithTag("Catcher");
            var custom = GameObject.FindGameObjectsWithTag("Custom_Runner");

			foreach (var potato in runners)
			{
				potato.transform.Find("CanvasHealthBar").gameObject.SetActive(false);
			}

			foreach (var apple in catchers)
			{
				apple.transform.Find("CanvasHealthBar").gameObject.SetActive(false);
			}

            foreach (var lol in custom)
            {
                lol.transform.Find("CanvasHealthBar").gameObject.SetActive(false);
            }
			
			waiter = 0;
		}
	
		
	}

	public bool IsGameFinished()
	{
		if (!BattleRoyale && Timeout >= GameDuration)
		{
			winners = "Runners";
			return true;
		}

		var activePlayers = 0;

		var runners = GameObject.FindGameObjectsWithTag("Runner");
        var custom = GameObject.FindGameObjectsWithTag("Custom_Runner");
        foreach (var douche in runners)
		{
			if (douche.GetComponent<NavMeshAgent>().enabled || douche.GetComponent<CharacterController>().detectCollisions)
				activePlayers++;
		}
        foreach (var lol in custom)
        {
            if (lol.GetComponent<NavMeshAgent>().enabled || lol.GetComponent<CharacterController>().detectCollisions)
                activePlayers++;
        }

        if (!BattleRoyale)
        {
	        winners = "Catchers";
			return activePlayers <= 0;
		}
		
		
		var catchers = GameObject.FindGameObjectsWithTag("Catcher");
		foreach (var douche in catchers)
		{
			if (douche.GetComponent<NavMeshAgent>().enabled || douche.GetComponent<CharacterController>().detectCollisions)
				activePlayers++;
		}

		if (activePlayers == 1)
		{
			winners = "lastGuyAlive";
			return true;
		}

		return false;
	}

	public string GetHighestScorePlayer()
	{
		var players = PhotonNetwork.playerList;
		int bestscore = 0;
		string bestname = "someone";
		foreach (var player in players)
		{
			if (player.GetScore() >= bestscore)
				bestname = player.name;
		}

		return bestname;
	}
}
