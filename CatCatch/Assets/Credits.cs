﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Credits : MonoBehaviour
{

    public float speed = 0.2f;

    void Start()
    {
        // init text here, more space to work than in the Inspector (but you could do that instead)
        TextMeshProUGUI mText = gameObject.GetComponent<TextMeshProUGUI>();
        var creds = "Unity3D\n";
        creds += "Unity3D Asset Store\n";
        creds += "JetBrains Rider\n";
        creds += "Microsoft Visual Studio\n";
        creds += "Microsoft Visual Studio Code\n";
        creds += "Adobe Photoshop\n";
        creds += "Adobe Premiere Pro\n";
        creds += "TurboSquid\n";
        creds += "Textures.com\n";
        creds += "Matteo Doudin\n";
        creds += "MuseScore\n";
        creds += "Photon Networking\n";
        creds += "OVH\n";
        creds += "Bootstrap\n";
        creds += "Django\n";
        creds += "Epiquote\n";
        creds += "Seirl et Dettorer";

        mText.text = creds;
    }


    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector3.up * Time.deltaTime * speed);
    }
}

