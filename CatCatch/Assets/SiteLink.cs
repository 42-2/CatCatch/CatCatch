﻿using System.Collections;
using UnityEngine;
using UnityEngine.Networking;

public class SiteLink : MonoBehaviour
{
    private const string SiteUrl = "http://www.catcatch.ovh/api/";

    private string _username;
    private string _mode;
    private string _score;
    private string _won;

    /*
     * Sends scores to the server
     * username: the user's username, matching the website
     * mode:
     *     - 1 is solo (won't do nothing, as user is not connected)
     *     - 2 is multiplayer
     *     - 3 is battle royale
     * score: the user's score (even if he didn't win)
     * won: did the user win ?
     */
    public void SendScores(string username, int mode, int score, bool won)
    {
        if (mode == 1) return;
        //Debug.Log("SendingScores");
        _username = username;
        _mode = mode.ToString();
        _score = score.ToString();
        _won = won.ToString();
        StartCoroutine(RequestScoreSending());
    }

    private IEnumerator RequestScoreSending()
    {
        var form = new WWWForm();
        form.AddField("username", _username);
        form.AddField("mode", _mode);
        form.AddField("score", _score);
        form.AddField("won", _won);
        //Debug.Log(form);
        //Debug.Log(_username);
        //Debug.Log(_mode);
        //Debug.Log(_score);
        //Debug.Log(_won);

        using (var www = UnityWebRequest.Post(SiteUrl + "scores.php", form))
        {
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                //Debug.Log(www.error);
            }
            else
            {
                var response = JsonUtility.FromJson<JsonSendScores>(www.downloadHandler.text);
                if (response.status_message == "Saved")
                {
                    //Debug.Log("Scores successfully saved");
                }
                else
                {
                    //Debug.Log("Scores sending response: " + response.status_message);
                }
            }
        }
    }
}


public class JsonSendScores
{
    public string status;
    public string status_message;
    public string data;
}
