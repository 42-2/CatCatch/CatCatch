﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TimeOut : MonoBehaviour {

    public TextMeshProUGUI mText;
    public float time;


	// Use this for initialization
	void Start () {

        TextMeshProUGUI mText = gameObject.GetComponent<TextMeshProUGUI>();


    }
	
	// Update is called once per frame
	void Update () {
        time = Mathf.RoundToInt(GetComponent<NetworkManager>().TimeLeft);
        time = time < 0 ? 0 : time;
        mText.text = time.ToString();
        
        
		
	}
}
