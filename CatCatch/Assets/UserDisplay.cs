﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UserDisplay : MonoBehaviour {

    public TMP_Text CaughtText;
    public GameObject CaughtUI;
    
    void CaughtDisplay(GUILayout text)
    {
        CaughtUI.SetActive(true);
        CaughtText.text = text.ToString();
        Invoke("DisableCaughtDisplay", 5);
    }

    void DisableCaughtDisplay()
    {
        CaughtUI.SetActive(false);
    }
}
