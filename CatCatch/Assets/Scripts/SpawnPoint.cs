﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class SpawnPoint : MonoBehaviour

{
    // teamId determines which team you're in
    // 0 is for the runners
    // 1 is for the catchers
    public int TeamId = 0;
}