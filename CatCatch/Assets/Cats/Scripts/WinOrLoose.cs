﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinOrLoose : MonoBehaviour
{
	public bool winner = false;
	private NetworkManager _networkManager;
	
	// Use this for initialization
	void Start () 
	{
		_networkManager = FindObjectOfType<NetworkManager>();
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (_networkManager.GameIsOver)
		{
			switch (_networkManager.winners)
			{
					case "Runners":
						if (CompareTag("Runner"))
							winner = true;
						break;
					case "Catchers":
						if (!CompareTag("Catcher"))
							break;
						if (GetComponent<NetworkCharacter>().Username == _networkManager.GetHighestScorePlayer())
							winner = true;
						break;
					case "lastGuyAlive":
						if (GetComponent<CharacterController>().detectCollisions)
							winner = true;
						break;
			}
		}
	}
}
