﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SocialPlatforms.Impl;

public class NetworkCharacter : Photon.MonoBehaviour
{
	private Vector3 _realPosition = Vector3.zero;
	private Quaternion _realRotation = Quaternion.identity;
	private Vector3 _realScale = Vector3.zero;
	public string Username;
	public int Score;

	private Animator _anim;

	//private float _lastUpdateTime;

	// Use this for initialization
	void Start ()
	{
		_anim = GetComponent<Animator>();
		Username = PhotonNetwork.playerName;
		Score = PhotonNetwork.player.GetScore();
	}
	
	// Update is called once per frame
	void Update () {
		if (!photonView.isMine)
		{
			transform.position = Vector3.Lerp(transform.position, _realPosition, 0.2f); 
			transform.rotation = Quaternion.Lerp(transform.rotation, _realRotation, 0.2f);
			transform.localScale = Vector3.Lerp(transform.localScale, _realScale, 0.2f);
		}
	}

	public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
	{
		if (stream.isWriting)
		{
			Score = PhotonNetwork.player.GetScore();
			//This is OUR player. We send our current position
			stream.SendNext(transform.position);
			stream.SendNext(transform.rotation);
			stream.SendNext(transform.localScale);
			stream.SendNext(_anim.GetFloat("Speed"));
			stream.SendNext(_anim.GetBool("Jumping"));
			stream.SendNext(Username);
			stream.SendNext(Score);
		}
		else
		{
			//This is someone else. We receive their position.
			_realPosition = (Vector3) stream.ReceiveNext();
			_realRotation = (Quaternion) stream.ReceiveNext();
			_realScale = (Vector3) stream.ReceiveNext();
			_anim.SetFloat("Speed", (float) stream.ReceiveNext());
			_anim.SetBool("Jumping", (bool) stream.ReceiveNext());
			Username = (string) stream.ReceiveNext();
			Score = (int) stream.ReceiveNext();
		}
	}
}