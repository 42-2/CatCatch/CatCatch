﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ListItemWinOrLoose : MonoBehaviour
{
	[SerializeField] Text winnerText;

	public void Setup(string winText)
	{
		winnerText.text = winText;
	}
}
