﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomCatVars : MonoBehaviour
{

	public int itemHead;
	public int itemBody;
	public int itemTexture;
	
	// Use this for initialization
	void Start ()
	{
		itemTexture = PlayerPrefs.GetInt("itemTexture");
		itemBody = PlayerPrefs.GetInt("itemBody");
		itemHead = PlayerPrefs.GetInt("itemHead");
	}
	
	public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
	{
		if (stream.isWriting)
		{
			//This is OUR player. We send our current position
			stream.SendNext(itemBody);
			stream.SendNext(itemHead);
			stream.SendNext(itemTexture);
		}
		else
		{
			//This is someone else. We receive their position.
			itemBody = (int) stream.ReceiveNext();
			itemHead = (int) stream.ReceiveNext();
			itemTexture = (int) stream.ReceiveNext();
		}
	}

}
