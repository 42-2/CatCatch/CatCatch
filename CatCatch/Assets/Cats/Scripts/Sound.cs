﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class Sound : MonoBehaviour {

    private Animator _anim;
    private AudioSource _audio;
    public AudioClip clipRUN;
    public AudioClip clipWalk;

    private bool walking = false;
    private bool running = false;

    void Start () {
        _anim = gameObject.GetComponent<Animator>();
        _audio = gameObject.GetComponent<AudioSource>();
        
    }
	
	void Update () {
        var speed = _anim.GetFloat("Speed"); // can be 0 (no sound at all) or between 0.1 and 12 (walking sound) or superior to 12 (running sound)
        if (speed <= 12 && speed > 0.1 && !walking)
        {
            walking = true;
            running = false;
            _audio.clip = clipWalk;
            _audio.Play();
        }
        else if (speed > 12 && !running)
        {
            running = true;
            walking = false;
            _audio.clip = clipRUN;
            _audio.Play();
        }
        else if (speed == 0)
        {
            running = false;
            walking = false;
            _audio.Stop();
        }




    }
}
