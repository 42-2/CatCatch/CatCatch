﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraCollider : MonoBehaviour
{
    public GameObject Cam1;
    public GameObject Cam2;
    private bool Exit = false;
    private int Cooldown = 30;

   

    private void OnTriggerStay(Collider other)
    {
        Cam1.SetActive(false);
        Cam2.SetActive(true);
        //Debug.Log("Stay");
        Exit = false;
        
    }
    private void OnTriggerExit(Collider other)
    {
        Exit = true;
        //Debug.Log("Exit");
    }

    private void Update()
    {
        if (Exit)
        {
            if (Cooldown <= 0)
            {
                Cam1.SetActive(true);
                Cam2.SetActive(false);
                Cooldown = 30;
            }
            else
            {
                Cooldown--;
            }
        }
    }



}
    

    
