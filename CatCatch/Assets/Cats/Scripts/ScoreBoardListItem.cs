﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreBoardListItem : MonoBehaviour {

	[SerializeField] Text usernameText;
	[SerializeField] Text killsText;
    private bool BattleRoyale;
    private int GameMode;


    public void Setup(string username, int kills)
	{
        GameMode = PlayerPrefs.GetInt("GameMode");
        //Debug.Log(GameMode);

        if(GameMode == 3)
        {
            if(kills == -1)
            {
                usernameText.text = username;
                killsText.text = "Kills : " + "killed";
            }
            else
            {
                usernameText.text = username;
                killsText.text = "Kills : " + kills;
            }
        }


            
        else
        {

            if (kills == -1)
            {
                usernameText.text = username;
                killsText.text = "Catches : " + "caught";
            }
            else
            {
                usernameText.text = username;
                killsText.text = "Catches : " + kills;
            }
        }
	}
}
