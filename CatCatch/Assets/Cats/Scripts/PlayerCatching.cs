﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using TMPro;

public class PlayerCatching : MonoBehaviour
{
	/*
     * THIS IS WHERE WE ARE CATCHING
     */
	private float _catchingDistance = 5f;
	private bool isAI;
	private bool _AIwantsToCatch = false;
	private EnnemyController _ennemyController;
	private Transform target;

    


    // Update is called once per frame
    void Update ()
	{
		if(GetCloserAI() != null)
			target = GetCloserAI().transform;
		
		isAI = gameObject.GetComponent<NavMeshAgent>().enabled;
		if (isAI)
		{
			_ennemyController = GetComponent<EnnemyController>();
		}
		if (transform.gameObject.CompareTag("Catcher") && (Input.GetButtonDown("Fire1") || _AIwantsToCatch))
		{
			// Player wants to catch
			Catch();
		}
	}
	
	void Catch()
	{
		if (isAI)
		{
			if (_ennemyController.Target != null) 
				_ennemyController.Target.GetComponent<PlayerCatching>().GetComponent<PhotonView>().RPC("Caught", PhotonTargets.All, PhotonNetwork.playerName);
		}
		else
		{
			if (target != null && Vector3.Distance(transform.position, target.transform.position) <= 5)
			{
				if (target.CompareTag("Runner") && target != null)
				{
                    PhotonNetwork.player.AddScore(1);
                    //Debug.Log("We caught : " + target);
					target.transform.GetComponent<PlayerCatching>().GetComponent<PhotonView>().RPC("Caught", PhotonTargets.All, PhotonNetwork.playerName);
				}
				else if(target != null)
				{
					//Debug.Log("You can't catch a catcher");
				}
			}
			else
			{
				var ray = new Ray(new Vector3(transform.position.x, transform.position.y+1, transform.position.z), transform.forward);
				var hitInfo = FindClosestCatchInfo(ray, _catchingDistance);
				if (!hitInfo.Equals(new RaycastHit()))
				{
					//Debug.Log("We hit    " + hitInfo.collider.name);

					if (hitInfo.transform.CompareTag("Runner") && hitInfo.transform.GetComponent<CharacterController>().detectCollisions)
                    {
                        PhotonNetwork.player.AddScore(1);
                        hitInfo.transform.GetComponent<PlayerCatching>().GetComponent<PhotonView>().RPC("Caught", PhotonTargets.All, PhotonNetwork.playerName);
						//Debug.Log("We caught    " + hitInfo.collider.name);
						GUILayout.Label("We caught " + hitInfo.collider.name); // TODO: make it pretty
					}
					else if (hitInfo.transform.CompareTag("Catcher"))
					{
						GUILayout.Label("You cannot catch a catcher!"); // TODO: make it pretty
						//Debug.Log("You cannot catch a catcher!"); // TODO: make it pretty
					}
				}
			}
		}
	}

	public GameObject GetCloserAI()
	{
        try
        {
            GameObject[] players = GameObject.FindGameObjectsWithTag("Runner");

            if (players.Length == 0)
            {
                return null;
            }
            var AIs = new List<GameObject>();

            for (int i = 0; i < players.Length; i++)
            {
                if (players[i].GetComponent<NavMeshAgent>().enabled)
                {
                    AIs.Add(players[i]);
                }
            }

            if (AIs.Count == 0)
            {
                return null;
            }

            if (AIs.Count == 1)
            {
                return AIs[0];
            }

            if (AIs.Count != 0)
            {
                var closer = AIs[0];
                for (int i = 1; i < AIs.Count; i++)
                {
                    if (Vector3.Distance(transform.position, AIs[i].transform.position) <= Vector3.Distance(transform.position, closer.transform.position))
                    {
                        closer = AIs[i];
                    }
                }

                return closer;
            }
        }
        catch (System.Exception)
        {
            return null;
        }

		return null;

	}
	
	private RaycastHit FindClosestCatchInfo(Ray ray, float maxDistance)
	{
		var hits = Physics.RaycastAll(ray, maxDistance); //max distance
		var closestHit = new RaycastHit();
		var distance = -1f;

		foreach (var hit in hits)
		{
			if (hit.transform != transform && hit.distance < distance || distance == -1f)
			{
				closestHit = hit;
				distance = hit.distance;
			}
		}
		return closestHit;
	}


	/*
	 * THIS IS WHERE WE ARE CAUGHT
	 */
	[PunRPC]
	public void Caught(string catcher)
	{
		if (CompareTag("Runner"))
		{
			//PhotonNetwork.player.SetScore(-1);
			if (!gameObject.GetComponent<NavMeshAgent>().enabled) // if we are not an AI
			{
				SpawnWaitingCamera();
				//Debug.Log("You were caught by " + catcher);
                //Debug.Log(PlayerPrefs.GetString("deathText"));
			}
			else if(gameObject != null)
			{
				//Debug.Log("Trying to destroy object :" + gameObject);
				PhotonNetwork.Destroy(gameObject);
			}
		}
	}

	private void SpawnWaitingCamera()
	{
		GetComponent<Spell>().enabled = false;
        GetComponent<AudioSource>().enabled = false;
        GetComponent<Sound>().enabled = false;
        transform.Find("Cat_Lite").gameObject.SetActive(false);
		transform.Find("Rig_Cat_Lite").gameObject.SetActive(false);
		
		GetComponent<PlayerCatching>().enabled = false;
        GetComponent<CharacterController>().detectCollisions = false;
		transform.Find("SpellUI").gameObject.SetActive(false);
		//transform.Find("CanvasPauseMenu").gameObject.SetActive(false);
		transform.Find("CanvasUserName").gameObject.SetActive(false);
		var edgarSmallCat = GameObject.FindGameObjectsWithTag("loledgar");
		foreach (var go in edgarSmallCat) go.SetActive(false);
	}

	public bool AIwantsToCatch
	{
		get { return _AIwantsToCatch; }
		set { _AIwantsToCatch = value; }
	}
}
