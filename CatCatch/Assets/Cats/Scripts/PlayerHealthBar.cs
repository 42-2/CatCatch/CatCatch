﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealthBar : MonoBehaviour 
{
	
	private float healthPct;
	[SerializeField] private RectTransform healthBarFill;
	
	// Use this for initialization
	/*void Start ()
	{
		
	}*/
	
	// Update is called once per frame
	void Update ()
	{
		healthPct = GetComponentInParent<PlayerBattleRoyale>().Life / 5f;
		healthBarFill.localScale = new Vector3(healthPct, 1f, 1f);
	}
}
