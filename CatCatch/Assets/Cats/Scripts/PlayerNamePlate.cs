﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;



public class PlayerNamePlate : MonoBehaviour {
	
	[SerializeField]
	private Text usernameText;

	private string[] botNames =
	{
		"TotallyNotABot",
		"AgentSmith",
		"Terminator",
		"xXDarkSasuke67Xx",
		"Coming4U",
		"CoolDude99",
		"Garfield",
		"Catou",
		"BetterThanU",
		"Neptune",
		"Schrodinger",
		"PianoCat",
		"GrumpyCat",
		"NyanCat",
		"AristoCat"
	};

    private string username;
	
    // Use this for initialization
    void Start ()
	{
        //usernqme = GetComponentInParent<NetworkCharacter>().Username;

        if (!GetComponentInParent<NavMeshAgent>().enabled)
			usernameText.text = GetComponentInParent<NetworkCharacter>().Username;
            
		else
		{
			username = botNames[Random.Range(0, botNames.Length)];
		}
	}
	
	// Update is called once per frame
	void Update () {
        if (!GetComponentInParent<NavMeshAgent>().enabled)
            usernameText.text = GetComponentInParent<NetworkCharacter>().Username;
        else
        {
            usernameText.text = username;
        }
    }
}
