﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

public class ScoreActivater : MonoBehaviour 
{

	[SerializeField] public GameObject scoreboard;
	private NetworkManager _networkManager;
	private PauseMenu _pauseMenu;

	private void Start()
	{
		_networkManager = FindObjectOfType<NetworkManager>();
		_pauseMenu = FindObjectOfType<PauseMenu>();
	}

	void Update ()
	{
		if (!_pauseMenu.GameIsPaused && (Input.GetKeyDown(KeyCode.Tab) || _networkManager.GameIsOver))
		{
			scoreboard.SetActive(true);
		}
		if(Input.GetKeyUp(KeyCode.Tab) && !_networkManager.GameIsOver)
		{
			scoreboard.SetActive(false);
		}

		if (_networkManager.GameIsOver)
		{
			var runners = GameObject.FindGameObjectsWithTag("Runner");
			var catchers = GameObject.FindGameObjectsWithTag("Catcher");
			var customs = GameObject.FindGameObjectsWithTag("Custom_Runner");
			foreach (var r in runners)
			{
				if(r.GetComponent<ScoreActivater>() != null)
					r.GetComponent<ScoreActivater>().scoreboard.SetActive(false);
			}
			foreach (var r in catchers)
			{
				if(r.GetComponent<ScoreActivater>() != null)
					r.GetComponent<ScoreActivater>().scoreboard.SetActive(false);
			}
			foreach (var r in customs)
			{
				if(r.GetComponent<ScoreActivater>() != null)
					r.GetComponent<ScoreActivater>().scoreboard.SetActive(false);
			}
			scoreboard.SetActive(true);
		}

		
	}
}
