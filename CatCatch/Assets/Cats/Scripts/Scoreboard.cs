﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class Scoreboard : MonoBehaviour 
{

	[SerializeField] GameObject scoreboardItem;
	[SerializeField] Transform playerscoreboardList;
	[SerializeField] GameObject winOrLooseItem;
	[SerializeField] GameObject quitButton;
	private NetworkManager _networkManager;
	private int timer = 0;

	private void Start()
	{
		_networkManager = FindObjectOfType<NetworkManager>();
	}


	private void Update()
	{
		timer++;

		if (timer >= 20)
		{
			foreach (Transform child in playerscoreboardList)
			{
				Destroy(child.gameObject);
			}
			//Debug.Log("update");
			var AllPlayers = PhotonNetwork.playerList;
			var SortedPlayers = AllPlayers.OrderByDescending(o => o.GetScore()).ToList();
			//loop through and set up a list item for each relevant to data
			foreach (var player in SortedPlayers)
			{
				GameObject itemGO = Instantiate(scoreboardItem, playerscoreboardList);
				ScoreBoardListItem item = scoreboardItem.GetComponent<ScoreBoardListItem>();
				if (item != null)
				{
                    item.Setup(player.name, player.GetScore());
				}
				else
				{
					//Debug.Log("mdr");
				}
			}

			if (_networkManager.GameIsOver)
			{
				GameObject itemwin = Instantiate(winOrLooseItem, playerscoreboardList);
				var listitem = winOrLooseItem.GetComponent<ListItemWinOrLoose>();
				if (listitem != null)
				{
					if(GetComponentInParent<WinOrLoose>().winner)
						listitem.Setup("You win !");
					else
						listitem.Setup("You loose !");
					
				}
				else
				{
					//Debug.Log("lel");
				}
				
				
				Instantiate(quitButton, playerscoreboardList);
			}
			timer = 0;
		}
		
	}
}
