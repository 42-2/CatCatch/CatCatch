﻿using System;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float Speed = 10f;
    public float JumpSpeed = 18f;
    public float Gravity = 40f;
    public float Acceleration = 1.4f;

    private Vector3 _direction = Vector3.zero;
    private float _verticalVelocity = 0f;

    private Animator _anim;
    private CharacterController _cc;
    

    // Use this for initialization
    private void Start()
    {
        _cc = GetComponent<CharacterController>();
        _anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    private void Update()
    {
        /*
         * BASIC MOVEMENT
         * forward/back & left/right
         */
        // Where we are headed to
        _direction = transform.rotation * new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));

        // Normalize the direction if we move diagonaly, so we don't move faster
        if (_direction.magnitude > 1f)
            _direction = _direction.normalized;

        // Set the animation speed, so the animation can change according to the speed
        _anim.SetFloat("Speed", _direction.magnitude * Speed);

        /*
         * RUNNING
         */
        // Start running
        if (Input.GetKeyDown(KeyCode.LeftShift)) Speed *= Acceleration;
        // Stop running
        if (Input.GetKeyUp(KeyCode.LeftShift)) Speed /= Acceleration;
        
        /*
         * JUMPING
         */
        if (_cc.isGrounded && Input.GetButton("Jump")) _verticalVelocity = JumpSpeed;
    }

    // FixedUpdate is called once per physics loop
    // Do all movement and other physics stuff here.
    private void FixedUpdate()
    {
        // Set the movement for x and z axis
        var distance = _direction * Speed * Time.deltaTime;

        if (_cc.isGrounded && _verticalVelocity < 0)
        {
            _anim.SetBool("Jumping", false);
            _verticalVelocity = Physics.gravity.y * Time.deltaTime;
        }
        else
        {
            if (Math.Abs(_verticalVelocity) > JumpSpeed * 0.75f)
            {
                _anim.SetBool("Jumping", true);
            }
            _verticalVelocity -= Gravity * Time.deltaTime; 
        }
        
        // Set the movement for y axis (if we're jumping and then falling back)
        distance.y = _verticalVelocity * Time.deltaTime;
        
        // Actually move
        _cc.Move(distance);
    }
}