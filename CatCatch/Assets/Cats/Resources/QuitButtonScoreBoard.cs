﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class QuitButtonScoreBoard : MonoBehaviour {

	public void QuitGame()
	{
		PhotonNetwork.LeaveRoom();
		PhotonNetwork.Disconnect();
		SceneManager.LoadScene("Menu");
	}
}
