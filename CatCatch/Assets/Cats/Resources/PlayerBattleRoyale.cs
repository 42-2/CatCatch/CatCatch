﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBattleRoyale : MonoBehaviour
{
	public int Life = 5;
	public int Range = 300000;
	private NetworkManager _networkManager;
	void Start () 
	{
		_networkManager = FindObjectOfType<NetworkManager>();
	}
	
	void Update () 
	{
		if (Input.GetButtonDown("Fire1") && _networkManager.GameStarted)
		{
			Shoot();
		}
	}

	public void Shoot()
	{
		//Debug.Log("Bang");
		var ray = new Ray(new Vector3(transform.position.x, transform.position.y+1, transform.position.z), transform.forward);
		var hitInfo = FindClosestCatchInfo(ray, Range);
		if (!hitInfo.Equals(new RaycastHit()) && (hitInfo.transform.CompareTag("Runner") || hitInfo.transform.CompareTag("Catcher")) && hitInfo.transform.GetComponent<CharacterController>().detectCollisions)
		{
			hitInfo.transform.GetComponent<PlayerBattleRoyale>().GetComponent<PhotonView>().RPC("Shot", PhotonTargets.All, PhotonNetwork.playerName);
			if (!hitInfo.transform.GetComponent<CharacterController>().detectCollisions)
			{
				PhotonNetwork.player.AddScore(1);
				Life = 5;
			}
				
		}
	}
	
	private RaycastHit FindClosestCatchInfo(Ray ray, float maxDistance)
	{
		var hits = Physics.RaycastAll(ray, maxDistance); //max distance
		var closestHit = new RaycastHit();
		var distance = -1f;

		foreach (var hit in hits)
		{
			if (hit.transform != transform && hit.distance < distance || distance == -1f)
			{
				closestHit = hit;
				distance = hit.distance;
			}
		}
		return closestHit;
	}

	[PunRPC]
	public void Shot(string shooter)
	{
		//Debug.Log("You were shot by" + shooter);

		Life--;
		
		if (Life <= 0) //you die
		{
			GetComponent<Spell>().enabled = false;
            GetComponent<AudioSource>().enabled = false;
            GetComponent<Sound>().enabled = false;
            transform.Find("Cat_Lite").gameObject.SetActive(false);
            transform.Find("Rig_Cat_Lite").gameObject.SetActive(false);
            
            GetComponent<PlayerBattleRoyale>().enabled = false;
            GetComponent<CharacterController>().detectCollisions = false;
            transform.Find("SpellUI").gameObject.SetActive(false);
            //transform.Find("CanvasPauseMenu").gameObject.SetActive(false);
            transform.Find("CanvasUserName").gameObject.SetActive(false);
			transform.Find("CanvasHealthBar").gameObject.SetActive(false);
            var edgarSmallCat = GameObject.FindGameObjectsWithTag("loledgar");
            foreach (var go in edgarSmallCat) go.SetActive(false);
			var hamdiCat = GameObject.FindGameObjectsWithTag("hamdi");
			foreach (var go in hamdiCat) go.SetActive(false);
		}
	}
}
