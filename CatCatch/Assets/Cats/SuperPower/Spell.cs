﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.AI;
using UnityEngine.UI;

public class Spell : MonoBehaviour
{
    //STUN
    public float StunDistance = 1000f;
    private float _oldSpeed;
    private float _oldSpeedBot;

    private float timer;
    public ParticleSystem spell;
    private PlayerMovement _stats;
    private Transform playerTransform;
    private float spellregen;
    public Image imageCooldown;
    public float cooldown;
    bool isCooldown;
    public Text spellregenText;
    public Text spellindicator;
    public int numberofspell;
    public string nameofspell;
    public RawImage rawimage;
    private bool _AIisUsingSpell;
    private NavMeshAgent agentSmith;
    private bool isAI;

    private void Start()
    {
        spellregen = 0;
        timer = 0;
        isCooldown = false;

        Texture2D spellImage = Resources.Load<Texture2D>(nameofspell);
        spellindicator.text = numberofspell.ToString();
        rawimage.texture = spellImage;
        AIisUsingSpell = false;
        isAI = GetComponent<NavMeshAgent>().enabled;
        agentSmith = GetComponent<NavMeshAgent>();

    }

    void Update()
    {
        isAI = GetComponent<NavMeshAgent>().enabled;
        timer += Time.deltaTime;
        spellregen += Time.deltaTime;
        spellregenText.text = ((int)spellregen).ToString();

        if (nameofspell == "Stun")
        {
            if (Input.GetKeyDown(KeyCode.E) && !isCooldown && numberofspell > 0)
            {
                timer = 0;
                isCooldown = true;
                numberofspell--;
                Stun();
            }
        }


        if (nameofspell == "Smokebomb")
        {
            if ((Input.GetKeyDown(KeyCode.E) || AIisUsingSpell)&& numberofspell > 0 && !isCooldown)
            {
                timer = 0;
                isCooldown = true;
                PhotonNetwork.Instantiate(spell.ToString().Split(' ').First(), transform.position, transform.rotation, 0);
                PhotonNetwork.Instantiate(spell.ToString().Split(' ').First(), transform.position, transform.rotation, 0);
                numberofspell--;
            }
        }
        else if (nameofspell == "Speed")
        {

            if ((Input.GetKeyDown(KeyCode.E) || AIisUsingSpell) && numberofspell > 0 && !isCooldown)
            {
                timer = 0;
                isCooldown = true;
                if (!isAI)
                {
                    _stats = GetComponent<PlayerMovement>();
                    _stats.Speed *= 2;
                }
                else
                {
                    agentSmith.speed *= 2;
                }
                
                Invoke("Speed", cooldown);
                numberofspell--;
            }
        }
        else if (nameofspell == "Scale")
        {

            if ((Input.GetKeyDown(KeyCode.E) || AIisUsingSpell) && numberofspell > 0 && !isCooldown)
            {
                timer = 0;
                isCooldown = true;
                playerTransform = transform;
                playerTransform.localScale /= 4;
                Invoke("Scale", cooldown);
                numberofspell--;
            }
        }
        if (timer >= cooldown)
        {
            isCooldown = false;
        }

        //Gestion de l'icone
        if (isCooldown)
        {
            imageCooldown.fillAmount += 1 / cooldown * Time.deltaTime;
            spellindicator.text = numberofspell.ToString();


            if (imageCooldown.fillAmount >= 0.99)
            {
                imageCooldown.fillAmount = 0;
                isCooldown = false;
            }
            
        }

        if(spellregen >= 30 && numberofspell < 3)
        {
            numberofspell++;
            spellindicator.text = numberofspell.ToString(); 
            spellregen = 0;
        }
        if(numberofspell >= 3)
        {
            spellregen = 0;
        }
    }

    void Stun()
    {
        var ray = new Ray(transform.Find("Main Camera").gameObject.transform.position, transform.Find("Main Camera").gameObject.transform.forward);
        var hitInfo = FindClosestCatchInfo(ray, StunDistance);
        if (!hitInfo.Equals(new RaycastHit()))
        {
            //Debug.Log("We hit    " + hitInfo.collider.name);
            if (hitInfo.transform.CompareTag("Runner") || hitInfo.transform.CompareTag("Catcher"))
            {
                if (isAI)
                {
                    hitInfo.transform.GetComponent<Spell>().GetComponent<PhotonView>().RPC("StunnedBot", PhotonTargets.All, PhotonNetwork.playerName);
                    //Debug.Log("We stun    " + hitInfo.collider.name);
                    GUILayout.Label("We stun " + hitInfo.collider.name); // TODO: make it pretty
                }
                else
                { 
                    hitInfo.transform.GetComponent<Spell>().GetComponent<PhotonView>().RPC("Stunned", PhotonTargets.All, PhotonNetwork.playerName);
                    //Debug.Log("We stun    " + hitInfo.collider.name);
                    GUILayout.Label("We stun " + hitInfo.collider.name); // TODO: make it pretty
                }
            }
        }
    }

    private RaycastHit FindClosestCatchInfo(Ray ray, float maxDistance)
    {
        var hits = Physics.RaycastAll(ray, maxDistance); //max distance
        var closestHit = new RaycastHit();
        var distance = -1f;

        foreach (var hit in hits)
        {
            if (hit.transform != transform && hit.distance < distance || distance == -1f)
            {
                closestHit = hit;
                distance = hit.distance;
            }
        }
        return closestHit;
    }

    [PunRPC]
    public void Stunned(string runner)
    {
        _oldSpeed = GetComponent<PlayerMovement>().Speed;
        GetComponent<PlayerMovement>().Speed = 0;
        Invoke("StopStun", 2);
        //Debug.Log("You were stunned by " + runner);
        GUILayout.Label("You were stunned by " + runner); // TODO: make it pretty   
    }

    [PunRPC]
    public void StunnedBot(string runner)
    {
        _oldSpeedBot = agentSmith.speed;
        agentSmith.speed = 0;
        Invoke("StopStunBot", 2);   
        //Debug.Log("You were stunned by " + runner);
        GUILayout.Label("You were stunned by " + runner); // TODO: make it pretty   
    }

    void Scale()
    {
        playerTransform.localScale *= 4;
    }

    void Speed()
    {
        if (isAI)
            agentSmith.speed /= 2;
        else
        _stats.Speed /= 2;
    }

    void StopStun()
    {
        GetComponent<PlayerMovement>().Speed = _oldSpeed;
    }

    void StopStunBot()
    {
        agentSmith.speed = _oldSpeedBot;
    }

    public bool AIisUsingSpell
    {
        get { return _AIisUsingSpell; }
        set { _AIisUsingSpell = value; }
    }
}
