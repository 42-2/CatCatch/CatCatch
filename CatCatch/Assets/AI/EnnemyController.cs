﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using ProBuilder2.Common;
using UnityEngine;
using UnityEngine.AI;

public class EnnemyController : MonoBehaviour
{
	public float lookRadius = 30f;

	GameObject[] targets;
	Transform target;
	private GameObject[] wayPoints;
	private int targetedWayPoint;
	private Animator _anim;
	private float distance;
	private Spell spellScript;
	private PlayerCatching _playerCatching;
	

	private NavMeshAgent agentSmith;
	// Use this for initialization
	void Start ()
	{
		////Debug.Log("Welcome to the");
		spellScript = GetComponent<Spell>();
		_playerCatching = GetComponent<PlayerCatching>();
		targets = GameObject.FindGameObjectsWithTag("Runner");
		wayPoints = GameObject.FindGameObjectsWithTag("WayPoint");
		agentSmith = GetComponent<NavMeshAgent>();
		targetedWayPoint = Random.Range(0, wayPoints.Length -1);
		_anim = GetComponent<Animator>();
		_anim.SetFloat("Speed", 14f);
		targetedWayPoint = Random.Range(0, wayPoints.Length -1);
		////Debug.Log(gameObject + "targeted : " + wayPoints[targetedWayPoint]);
		agentSmith.SetDestination(wayPoints[targetedWayPoint].transform.position);
		distance = 1000;
	}
	
	// Update is called once per frame
	void Update ()
	{
		////Debug.Log("Hotel California");
		if (CompareTag("Catcher"))
		{
			targets = GameObject.FindGameObjectsWithTag("Runner");
			
			if (GetCloserTarget() == null) 
			{
				if (Vector3.Distance(wayPoints[targetedWayPoint].transform.position, transform.position) <= 3)
				{
					targetedWayPoint = Random.Range(0, wayPoints.Length -1);
					////Debug.Log(gameObject + "targeted : " + wayPoints[targetedWayPoint]);
				}
				agentSmith.SetDestination(wayPoints[targetedWayPoint].transform.position);
			}
			else
			{
				target = GetCloserTarget().transform;

				if (target != null)
					distance = Vector3.Distance(target.position, transform.position);
				else
					distance = 100;
				
				_playerCatching.AIwantsToCatch = distance <= 5;
				if (distance <= lookRadius && target != null)
				{
					agentSmith.SetDestination(target.position);
				}
				else
				{
					if (Vector3.Distance(wayPoints[targetedWayPoint].transform.position, transform.position) <= 3)
					{
						targetedWayPoint = Random.Range(0, wayPoints.Length -1);
						//Debug.Log(gameObject + "targeted : " + wayPoints[targetedWayPoint]);
					}
					agentSmith.SetDestination(wayPoints[targetedWayPoint].transform.position);
				}
			}
		}
		else
		{
			targets = GameObject.FindGameObjectsWithTag("Catcher"); 

			if (GetCloserTarget() != null)
			{
				target = GetCloserTarget().transform;
				distance = Vector3.Distance(target.position, transform.position);
			}
			else
			{
				distance = 100;
			}

			if (Vector3.Distance(wayPoints[targetedWayPoint].transform.position, transform.position) <= 3)
			{
				targetedWayPoint = Random.Range(0, wayPoints.Length -1);
				//Debug.Log(gameObject + "targeted : " + wayPoints[targetedWayPoint]);
			}
			agentSmith.SetDestination(wayPoints[targetedWayPoint].transform.position);
		}
		
		if (distance <= 20 && GetCloserTarget() != null)
		{
			spellScript.AIisUsingSpell = true;
		}
		else
		{
			spellScript.AIisUsingSpell = false;
		}
	}

	private void OnDrawGizmosSelected()
	{
		Gizmos.color = Color.red;
		Gizmos.DrawWireSphere(transform.position, lookRadius);
	}

	private GameObject GetCloserTarget()
	{
		////Debug.Log("number of targets:" + targets.Length);
		if (targets.Length == 0)
		{
			return null;
		}
		
		var activeTargets = new List<GameObject>();

		for (int i = 0; i < targets.Length; i++)
		{
			if (targets != null && targets[i] != null && targets[i].GetComponent<PlayerCatching>() != null && targets[i].GetComponent<PlayerCatching>().enabled)
			{
				activeTargets.Add(targets[i]);
			}
		}

		if (activeTargets.Count == 0)
		{
			return null;
		}
		
		GameObject closer = activeTargets[0];
		if (activeTargets.Count == 1)
		{
			return closer;
		}
		for (int i = 1; i < activeTargets.Count; i++)
		{
			if (Vector3.Distance(closer.transform.position, transform.position) >=
			    Vector3.Distance(activeTargets[i].transform.position, transform.position))
			{
				closer = activeTargets[i];
			}
		}

		return closer;
	}

	public Transform Target
	{
		get { return target; }
		set { target = value; }
	}

}
